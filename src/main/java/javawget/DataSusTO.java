package javawget;

import java.util.ArrayList;

public class DataSusTO {
	
	
	private String urlToDownload;
	private String pathOutputFile;
	private ArrayList<String> ftpList;
	
	public String getUrlToDownload() {
		return urlToDownload;
	}
	public void setUrlToDownload(String urlToDownload) {
		this.urlToDownload = urlToDownload;
	}
	public String getPathOutputFile() {
		return pathOutputFile;
	}
	public void setPathOutputFile(String pathOutputFile) {
		this.pathOutputFile = pathOutputFile;
	}
	public ArrayList<String> getFtpList() {
		return ftpList;
	}
	public void setFtpList(ArrayList<String> ftpList) {
		this.ftpList = ftpList;
	}

}
