package javawget;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

public class WGet {
	
	final static Logger logger = Logger.getLogger(WGet.class);

	private DataSusTO dataSusTO;		

	public static void main(String[] args) {
		new WGet().execute();
	}
	
	private void execute() {
		String errorHtmlPathNotFound = "Erro ao tentar ler o diretório de arquivos html!";
		Path pathHtmlDirectory = Paths.get("//Users//alexandre1202//Documents//optum//html//");
		
		try {
			Files.list(pathHtmlDirectory).forEach(path -> {
				downloadDBCFile(path);
			});
		} catch (IOException e) {
			logger.error(errorHtmlPathNotFound + " - " + e.getMessage());
		}		
	}
	
	private void downloadDBCFile(Path htmlPath) {
		String ftpDatasusUrl = "ftp.datasus.gov.br";
		HtmlLinkExtraction htmlTagExtraction = new HtmlLinkExtraction();
		String errorFileNotFound = "Arquivo html nao encontrato! ";
		String outputDirectory = "/Users/alexandre1202/Documents/optum/datasus/dbcfiles/";
		Path pathFileNameItem;
		String fullPathFileName;
		ReadableByteChannel readableByteChannelToFile = null;
		FileOutputStream fos = null;
		URL urlToDownload = null;
		String fullFTPURL = null;
		
		try (BufferedReader bufferReaderHtml = Files.newBufferedReader(htmlPath, StandardCharsets.ISO_8859_1)) {
			ArrayList<HTMLLinkElement> linkElements = null;
			
			//Get the ftp List
			String currentLine = null;
			String fileNameItem = null;
			long downloadCounter = 0;
			while((currentLine = bufferReaderHtml.readLine()) != null) {
				if (currentLine.contains(ftpDatasusUrl)) {
					linkElements = htmlTagExtraction.extractHTMLLinks(currentLine);
					for (HTMLLinkElement ftpList : linkElements) {
						if (ftpList.getLinkAddress().toString().substring(0, 4).equals("ftp:")) {							
							urlToDownload = new URL(ftpList.getLinkAddress());
							fullFTPURL = ftpList.getLinkAddress().toString();
							fileNameItem = fullFTPURL.substring(fullFTPURL.lastIndexOf("/")+1, fullFTPURL.length());
							fullPathFileName = outputDirectory + fileNameItem;
							pathFileNameItem = Paths.get(fullPathFileName);
							if (! Files.exists(pathFileNameItem) ) {
								readableByteChannelToFile = Channels.newChannel(urlToDownload.openStream());
								fos = new FileOutputStream(fullPathFileName);
								fos.getChannel().transferFrom(readableByteChannelToFile, 0, Long.MAX_VALUE);
								fos.close();
								readableByteChannelToFile.close();
							}
							logger.info(" [( " + htmlPath + ")-" + ++downloadCounter + " ] "+ fullFTPURL + " downloaded in " + outputDirectory + fileNameItem);
							urlToDownload = null;
							fullFTPURL = null;
						}
					}
				}
			}
			
		} catch (IOException ioe) {
			logger.error("ERRO NO DOWNLOAD: " + fullFTPURL + " - " + errorFileNotFound + " - " + ioe.getMessage());
		}
	}
	
}
